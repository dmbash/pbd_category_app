class ProductController < ApplicationController

  def create
    @product = Product.create(user_params)
    if @product.save
      redirect_to show_products_path(params[:product][:category_id])
    else
      render :new
    end
  end

  def destroy
    Product.find(params[:id]).destroy
    redirect_to show_products_path(params[:product][:category_id])
  end

  def new
    @product = Product.new
  end

  def edit
    @product = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])
    if @product.update_attributes(user_params)
      redirect_to show_products_path(params[:product][:category_id])
    end
  end

  def user_params
   @category = params[:category]
   params.require(:product).permit(:title, :manufacturer, :price, :category_id)
  end

end
