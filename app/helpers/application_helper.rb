# coding: utf-8
module ApplicationHelper

  def nested_categories(categories)
    categories.map do |category, sub_categories|
      render html: content_tag(:div, category.title, :class => "category") + cat_edit_button(category.id) + cat_delete_button(category.id) + cat_show_button(category.id) + content_tag(:div, nested_categories(sub_categories), :class => "sub_category")
    end.join.html_safe
  end

  def cat_new_button()
    content_tag(:button, link_to("+" , new_category_path), class: "plus_button app_button")
  end

  def cat_edit_button(id)
    content_tag(:button, link_to("Edit", edit_category_path(id)), class: "edit_button app_button")
  end

  def cat_delete_button(id)
    content_tag(:button, link_to("-", category_path(id), method: :delete, data: {confirm: "Вы уверены?"}), class: "minus_button app_button")
  end

  def cat_show_button(id)
    content_tag(:button, link_to("Show", show_products_path(id), method: :get), class: "show_button app_button")
  end

  def prod_new_button()
    content_tag(:button, link_to("+", new_product_path), class: "plus_button app_button")
  end

  def prod_edit_button(id)
    content_tag(:button, link_to("Edit", edit_product_path(id)), class: "edit_button app_button")
  end

  def prod_delete_button(id)
    content_tag(:button, link_to("-", product_path(id), method: :delete, data: {confirm: "Вы уверены?"}), class: "minus_button app_button")
  end

end
